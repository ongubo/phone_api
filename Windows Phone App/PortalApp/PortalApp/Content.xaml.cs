﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using PortalApp.Classes;
using Newtonsoft.Json.Linq;

namespace PortalApp
{
    public partial class Content : PhoneApplicationPage
    {
        string user;
        public int cnt = 0;
        private HttpClient httpClient;
        public HttpResponseMessage httpResponseMessage;


        List<Book> booksList;
        List<Exam> examList;
        List<Fee> feeList;
        DateTime today;

        Fee feeObject;
        public Content()
        {
            InitializeComponent();
            httpClient = new HttpClient();
            booksList = new List<Book>();
            examList = new List<Exam>();
            feeList = new List<Fee>();
            today = DateTime.Today;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.Text = "Populating data. Please wait";
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("student_id", out user))
            {
                int u = int.Parse(user);
                getStudentMarksAsync(u);
                getstudentFeeAsync(u);
                getstudentRecordsAsync(u);
                getStudentBookRecordsAsync(u);
            }
            
        }

        public async void getStudentMarksAsync(int id)
        {
            SetProgressIndicator(true);
            try
            {
                var student = await httpClient.GetAsync(new Uri(App.HOST_ADDRESS+"student/" + id + "/exams"));
                student.EnsureSuccessStatusCode();
                dynamic exams = JValue.Parse(await student.Content.ReadAsStringAsync());
                foreach (var exam in exams)
                {
                    examList.Add(new Exam(exam.id.ToString(), 
                                          exam.student_id.ToString(), 
                                          exam.year_of_study.ToString(), 
                                          exam.unit_code.ToString(), 
                                          exam.unit_name.ToString(), 
                                          exam.grade.ToString(), 
                                          exam.created_at.ToString())); 
                }
                lstexamsList.ItemsSource = examList;
            }
            catch (Exception ex)
            {
                MessageBox.Show( "Error = " + ex.HResult.ToString("X") + "  Message: " + ex.Message);
            }
            //SetProgressIndicator(false);
        }

        public async void getStudentBookRecordsAsync(int id)
        {
            try
            {
                var results = await httpClient.GetAsync(new Uri(App.HOST_ADDRESS + "student/" + id + "/books"));
                results.EnsureSuccessStatusCode();
                dynamic books = JValue.Parse(await results.Content.ReadAsStringAsync());
                foreach (var book in books)
                {
                    booksList.Add(new Book(book.id.ToString(),
                                            book.student_id.ToString(),
                                            book.book_name.ToString(),
                                            book.author.ToString(),
                                            book.ISBN.ToString(),
                                            book.publisher.ToString(),
                                            book.call_number.ToString(),
                                            book.due_date.ToString()
                                            ));
                }
                lstbookList.ItemsSource = booksList;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error = " + ex.HResult.ToString("X") + "  Message: " + ex.Message);
            }
            SetProgressIndicator(false);
        }
        public async void getstudentFeeAsync(int id)
        {
            feeObject = new Fee();
            try
            {
                var results = await httpClient.GetAsync(new Uri(App.HOST_ADDRESS+"student/" + id + "/fees"));
                results.EnsureSuccessStatusCode();
                dynamic fees = JValue.Parse(await results.Content.ReadAsStringAsync());
                foreach (var fee in fees)
                {
                    feeList.Add(new Fee(fee.id.ToString(),
                                        fee.student_id.ToString(),
                                        fee.date.ToString(),
                                        fee.type.ToString(),
                                        fee.transaction_number.ToString(),
                                        fee.description.ToString(),
                                        fee.amount.ToString(),
                                        fee.created_at.ToString()));
                }
                //feeObject.getFeeBalance();
                lstfeeList.ItemsSource = feeList;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error = " + ex.HResult.ToString("X") + "  Message: " + ex.Message);
            }
            SetProgressIndicator(false);
        }

      
        public async void getstudentRecordsAsync(int id)
        {
            try
            {
                var student = await httpClient.GetAsync(new Uri(App.HOST_ADDRESS+"student/" + id));
                student.EnsureSuccessStatusCode();
                dynamic record=JValue.Parse(await student.Content.ReadAsStringAsync());
                foreach (var records in record)
                {
                    txtStudentName.Text = records.first_name + " " + records.last_name;
                    txtStudentAdmission.Text = records.admission_number;
                    txtStudentemail.Text = records.user_name+".ac.ke";
                    txtStudentFaculty.Text = records.faculty;
                    txtStudentDepartent.Text = records.department;
                    txtStudentCourse.Text = records.course;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error = " + ex.HResult.ToString("X") + "  Message: " + ex.Message);
            }
            SetProgressIndicator(false);
        }
        public void SetProgressIndicator(bool isVisible)
        {
            
            SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
            SystemTray.ProgressIndicator.IsVisible = isVisible;
        }

        public void refreshAll_Click(object sender, EventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.Text = "Refreshing data.please wait";
            int u = int.Parse(user);

            examList.Clear();
            examList.Clear();


            getStudentMarksAsync(u);
            getstudentFeeAsync(u);
            getstudentRecordsAsync(u);
            getStudentBookRecordsAsync(u);
            mainPivot.SelectedIndex = 0;
        }

        public void refresFeeResult_Click(object sender, EventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.Text = "Refreshing fee records.please wait";
            int u = int.Parse(user);
            SetProgressIndicator(true);

            feeList.Clear();
            getstudentFeeAsync(u);
            mainPivot.SelectedIndex = 1;
        }

        public void refreshExamResults_Click(object sender, EventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.Text = "Refreshing exam records.please wait";
            int u = int.Parse(user);
            SetProgressIndicator(true);

            examList.Clear();
            getStudentMarksAsync(u);
            mainPivot.SelectedIndex = 0;
        }

        public void refreshBooks_Click(object sender, EventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.Text = "Refreshing library records.please wait";
            int u = int.Parse(user);
            SetProgressIndicator(true);

            booksList.Clear();
            getStudentBookRecordsAsync(u);
            mainPivot.SelectedIndex=2;
        }

        private void lstfeeList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PhoneApplicationService.Current.State["FeeItem"] = (Fee)lstfeeList.SelectedItem;
            NavigationService.Navigate(new Uri("/ViewIndividual.xaml?from=fee", UriKind.Relative));
        }

        private void lstexamsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PhoneApplicationService.Current.State["ExamItem"] = (Exam)lstexamsList.SelectedItem;
            NavigationService.Navigate(new Uri("/ViewIndividual.xaml?from=exams", UriKind.Relative));
        }

        private void lstbookList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PhoneApplicationService.Current.State["BookItem"] = (Book)lstbookList.SelectedItem;
            NavigationService.Navigate(new Uri("/ViewIndividual.xaml?from=books", UriKind.Relative));
        }
    }
}