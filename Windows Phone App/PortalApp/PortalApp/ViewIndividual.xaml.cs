﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PortalApp.Classes;

namespace PortalApp
{
    public partial class ViewInvividua_ : PhoneApplicationPage
    {
        string from;
     
        public ViewInvividua_()
        {
            InitializeComponent();
            Loaded += ViewInvividua__Loaded;
        }

        void ViewInvividua__Loaded(object sender, RoutedEventArgs e)
        {
            //var feeItem = PhoneApplicationService.Current.State["selectedfee"];
            //MessageBox.Show(feeItem.ToString());
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Fee feeItem;
            Book bookItem;
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("from", out from))
            {
                if (from.Equals("fee"))
                {
                    feeItem = (Fee)PhoneApplicationService.Current.State["FeeItem"];
                    txtHeader.Text = "Fee";
                    lbl1.Text = "Amount"; txt1.Text = feeItem.amount;
                    lbl2.Text = "Payment Date"; txt2.Text = feeItem.date;
                    lbl3.Text = "Type"; txt3.Text = feeItem.type;
                    lbl4.Text = "Transaction No"; txt4.Text = feeItem.transaction_number;
                }
                if (from.Equals("books"))
                {
                    txtHeader.Text = "Books";
                    bookItem = (Book)PhoneApplicationService.Current.State["BookItem"];
                    lbl1.Text = "Book Name"; txt1.Text = bookItem.book_name;
                    lbl2.Text = "ISBN"; txt2.Text = bookItem.ISBN;
                    lbl3.Text = "Publisher"; txt3.Text = bookItem.publisher;
                    lbl4.Text = "Author"; txt4.Text = bookItem.author;
                    lbl5.Text = "Call Number"; txt5.Text = bookItem.call_number;
                    lbl6.Text = "Due date"; txt6.Text = bookItem.due_date;
                }
                if (from.Equals("exams"))
                {
                    txtHeader.Text = "Exams";
                } 
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            if (from.Equals("fee"))
            {
                PhoneApplicationService.Current.State["FeeItem"] = null;
            }
            if (from.Equals("books"))
            {
                PhoneApplicationService.Current.State["BookItem"] = null;
            }
            if (from.Equals("exams"))
            {
                PhoneApplicationService.Current.State["ExamItem"] = null;
            }
            
        }
    }
}