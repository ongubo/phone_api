﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalApp.Classes
{
    class Sample
    {
        public Sample(string _id, string _fname,string _lName, string _username)
        {
            id = _id;
            first_name = _fname;
            last_name = _lName;
            user_name = _username;
        }

        public Sample()
        {
        }
        public string id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string user_name { get; set; }
        public string password { get; set; }
        public string admission_number { get; set; }
        public string course { get; set; }
        public string faculty { get; set; }
        public string department { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public object deleted_at { get; set; }
    }
}
