﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalApp.Classes
{
    class Book
    {
        public string id { get; set; }
        public string student_id { get; set; }
        public string book_name { get; set; }
        public string author { get; set; }
        public string ISBN { get; set; }
        public string publisher { get; set; }
        public string created_at { get; set; }
        public string call_number { get; set; }
        public string due_date { get; set; }
        public Book(string _id, string _student_id, string _book_name, string _book_author,string _isbn, string _publisher,string _callNumber,string _dueDate)
        {
            id = _id;
            student_id = _student_id;
            book_name = _book_name;
            author = _book_author;
            ISBN = _isbn;
            publisher = _publisher;
            call_number = _callNumber;
            due_date = _dueDate;
        }
    }
}
