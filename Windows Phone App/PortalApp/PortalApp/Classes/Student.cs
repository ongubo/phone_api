﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalApp
{
    class Student
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string user_name { get; set; }
        public string admission_number { get; set; }
        public string course { get; set; }
        public string faculty { get; set; }
        public string department { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public object deleted_at { get; set; }

        public Student(string _id, string _first_name, string _last_name, string _user_name, string _admission_number, string _course, string _faculty, string _department)
        {
            id = _id;
            first_name = _first_name;
            last_name = _last_name;
            user_name = _user_name;
            admission_number = _admission_number;
            course = _course;
            faculty = _faculty;
            department = _department;
        }
    }
}
