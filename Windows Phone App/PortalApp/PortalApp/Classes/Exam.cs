﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalApp
{
    class Exam
    {
        public string id { get; set; }
        public string student_id { get; set; }
        public string year_of_study { get; set; }
        public string unit_code { get; set; }
        public string unit_name { get; set; }
        public string grade { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public object deleted_at { get; set; }

        public Exam(string _id, string _studentId, string _year_of_study, string _unit_code, string _unit_name, string _grade, string _date)
        {
            id = _id;
            student_id = _studentId;
            year_of_study = _year_of_study;
            unit_code = _unit_code;
            unit_name = _unit_name;
            grade = _grade;
            created_at = _date;

        }
    }
}
