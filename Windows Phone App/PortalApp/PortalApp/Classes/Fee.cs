﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PortalApp.Classes
{
    class Fee
    {
        //Content content;
        public string id { get; set; }
        public string student_id { get; set; }
        public string date { get; set; }
        public string type { get; set; }
        public string transaction_number { get; set; }
        public string description { get; set; }
        public string amount { get; set; }
        public string created_at { get; set; }
        public string updated_at { get; set; }
        public object deleted_at { get; set; }

        public Fee(string _id, string _student_id, string _date, string _type, string _transaction_number, string _description, string _amount, string _created_at)
        {
            id = _id;
            student_id = _student_id;
            date = _date;
            type = _type;
            transaction_number = _transaction_number;
            description = _description;
            amount = _amount;
            created_at = _created_at;
        }

        public Fee()
        {
            // TODO: Complete member initialization
        }

       /* public double getFeeBalance()
        {
            double balance = 0;
            content = new Content();
            if (content.feeList.Count > 0)
            {
                foreach (var item in content.feeList)
                {
                    if (item.type == "INVOICE")
                    {
                        balance += double.Parse(item.amount);
                    }
                    if (item.type == "RECEIPT" || item.type == "ADJUSTMENT")
                    {
                        balance -= double.Parse(item.amount);
                    }

                }
            }
            return balance;
        }*/

    }
}
