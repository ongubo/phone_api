﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PortalApp.Resources;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using Newtonsoft.Json.Linq;

namespace PortalApp
{
    public partial class MainPage : PhoneApplicationPage
    {
        private string user_name;
        private string password;
        private HttpClient httpclient;
        private HttpResponseMessage response;
        // Constructor
        public MainPage()
        {
            InitializeComponent();
        }

        private async void handleLogin()
        { 
            response = new HttpResponseMessage();
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.Text = "Trying to log in.Please Wait";
            if (txtUsername.Text== ""){MessageBox.Show("Please enter your username");}
            if (txtPassword.Password == "") { MessageBox.Show("Please enter your password");}
            else
            {
                user_name = txtUsername.Text;
                password = txtPassword.Password;
                SetProgressIndicator(true);
                dynamic g =null;
                var urlParameters = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("user_name", user_name+"@students.jkuat"),
                        new KeyValuePair<string, string>("password", password)
                    };
                HttpContent content = new FormUrlEncodedContent(urlParameters);
                try
                {
                    httpclient = new HttpClient(new HttpClientHandler());
                    response = await httpclient.PostAsync(App.HOST_ADDRESS+"students", content);
                    g= JValue.Parse(await response.Content.ReadAsStringAsync());
                }
                catch (Exception)
                {
                    SetProgressIndicator(false);
                    MessageBox.Show("Whoops! Sorry.Something went wrong");
                    txtPassword.Password = "";
                    
                }
                try
                {
                    string isAllowed = g.Allowed;
                    string message = g.message;
                    App.USER_NAME = g.user;
                    if (response.IsSuccessStatusCode)
                    {
                       if (isAllowed == "1")
                       {
                           SetProgressIndicator(false);
                           NavigationService.Navigate(new Uri("/Content.xaml?student_id="+g.id, UriKind.Relative));
                       }
                       else
                       {
                           MessageBox.Show(message);
                           txtPassword.Password = "";
                       }
                    }
                }
                catch (Exception )
                {
                   // MessageBox.Show("Error = " + ex.HResult.ToString("X") + "  Message: " + ex.Message);
                }
                
            }
        }

        private void btnCancel_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            txtPassword.Password = "";
            txtUsername.Text = "";
            SetProgressIndicator(false); 
            
        }

        private void SetProgressIndicator(bool isVisible)
        {
            SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
            SystemTray.ProgressIndicator.IsVisible = isVisible;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            handleLogin();
        }
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            txtPassword.Password = "";
        }

        private void lnkSample_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/sampleData.xaml", UriKind.Relative));
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string uName=null;
            base.OnNavigatedTo(e);
            if (NavigationContext.QueryString.TryGetValue("name" ,out uName))
            {
                txtUsername.Text = uName;
                txtPassword.Password = "";
            }
        }
       

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}