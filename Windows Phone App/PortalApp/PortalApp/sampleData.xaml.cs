﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using PortalApp.Classes;

namespace PortalApp
{
    public partial class sampleData : PhoneApplicationPage
    {
        public int cnt = 0;
        private HttpClient httpClient;
        public HttpResponseMessage httpResponseMessage;
        List<Sample> dataList;
        public sampleData()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            httpClient = new HttpClient();
            SetProgressIndicator(true);
            SystemTray.ProgressIndicator.Text = "Obtaining student data. Please wait";
            getStudentsAsync();
        }
        public async void getStudentsAsync()
        {
             dataList= new List<Sample>();
            try
            {
                var students = await httpClient.GetAsync(new Uri(App.HOST_ADDRESS + "students"));
                System.Diagnostics.Debug.WriteLine(App.HOST_ADDRESS + "students");
                students.EnsureSuccessStatusCode();
                dynamic results = JValue.Parse(await students.Content.ReadAsStringAsync());
                foreach (var student in results)
                {
                    dataList.Add(new Sample(student.id.ToString(), student.first_name.ToString(), student.last_name.ToString(), student.user_name.ToString()));
                }
                samplesList.ItemsSource = dataList;
                SetProgressIndicator(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error = " + ex.HResult.ToString("X") + "  Message: " + ex.Message);
            }
        }
        private void ApplicationBarIconButton_Click_1(object sender, System.EventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
        public void SetProgressIndicator(bool isVisible)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
            SystemTray.ProgressIndicator.IsVisible = isVisible;
        }

        private void samplesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Sample x = new Sample();
            x=dataList.ElementAt(samplesList.SelectedIndex);
            NavigationService.Navigate(new Uri("/MainPage.xaml?name=" + x.last_name+"."+x.first_name, UriKind.Relative));
        }
       
    }
}