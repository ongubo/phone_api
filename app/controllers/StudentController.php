<?php

class StudentController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return User::all();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// $students = DB::table('students')->select('id', 'type', 'title', 'size', 'county', 'posted_by', 'created_at');
        
  //       if (Input::has('max_size')) {
  //           $students = $students->where('size', '<=', Input::get('max_size'));
  //       }
  //       return Response::JSON($students->get());
		return  (String) User::find($id)->fee_records;
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	public function getStudentRequest($id,$param)
	{
		//fees querying
		if ($param == 'fees' || $param == 'fee') {
			$fee=Fee::where('student_id', $id);
			if(Input::has('type')) {
				$fee = $fee->where('type',Input::get('type'));
			}
			return $fee->get();
		}

		//Exam querying
		if ($param == 'exams' || $param == 'exam' || $param == 'results') {
			$exam=Exam::where('student_id', $id);
			if(Input::has('grade')) {
				$exam=$exam->where('grade',Input::get('grade'));
			}
			return $exam->get();
		}

		//Books querying
		if ($param == 'library' || $param == 'books' || $param == 'book') {
			$book= Book::where('student_id', $id);
			if (Input::has('book_name')) {
				$book=$book->where('book_name',Input::get('book_name'));
			}
			if (Input::has('book_author')) {
				$book=$book->where('book_author',Input::get('book_author'));
			}
			if (Input::has('period_allowed')) {
				$book=$book->where('period_allowed',Input::get('period_allowed'));
			}
			if ($book==null || empty($book)) {
				return Response::JSON(array('Error' => 'No records found'));
			}
			return $book->get();
		}
		else {
			return Response::JSON(array('Error' => 'Invalid URL. Please check the URL and try again'));
		}
	}

	public function getStudent($id)//returns a given student
	{
		return User::find($id);
	}

	public function searchStudent()//finds a given student(s) based on url parameters
	{
		$student=null;
		if (Input::has('first_name')) {
			$student =User::where('first_name',Input::get('first_name'));
		}
		if (Input::has('last_name')) {
			$student =User::where('last_name',Input::get('last_name'));
		}
		if (Input::has('user_name')) {
			$student =User::where('user_name',Input::get('user_name'));
		}
		if ($student==null) {
			return Response::JSON(array('Error' => 'Invalid URL. Please check the URL and try again'));
		}
		return $student->get();
	}
}
