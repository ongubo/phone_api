<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', 'HomeController@showWelcome');

Route::resource('students','StudentController');

Route::resource('books','BookController');

Route::resource('exams','ExamController');

Route::resource('fees','FeeController');

Route::get('student/', array('uses' => 'StudentController@searchStudent'));

Route::get('student/{id}', array('uses' => 'StudentController@getStudent'));

Route::get('student/{id}/{param}', array('uses' => 'StudentController@getStudentRequest'));

