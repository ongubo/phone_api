<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialPortalApiDatabase extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('students', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('first_name',50);
			$table->string('last_name',50);
			$table->string('user_name',50);
			$table->string('password',50);
			$table->string('admission_number',20);
			$table->string('course',50);
			$table->string('faculty',50);
			$table->string('department',50);
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('fees', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('student_id')->unsigned();
			$table->foreign('student_id')->references('id')->on('students');
			$table->dateTime('date');
			$table->string('type',50);
			$table->string('transaction_number',50);
			$table->string('description',400);
			$table->decimal('amount',10,2);
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('exams', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('student_id')->unsigned();
			$table->foreign('student_id')->references('id')->on('students');
			$table->string('year_of_study',50);
			$table->string('unit_code',50);
			$table->string('unit_name',50);
			$table->string('grade',5);
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('librarybooks', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('student_id')->unsigned();
			$table->foreign('student_id')->references('id')->on('students');
			$table->string('book_name',50);
			$table->string('book_author',50);
			$table->string('period_allowed',50);
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('units', function(Blueprint $table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('unit_code',10);
			$table->string('unit_name',100);
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fees');
		Schema::drop('units');
		Schema::drop('librarybooks');
		Schema::drop('exams');
		Schema::drop('students');
	}

}
