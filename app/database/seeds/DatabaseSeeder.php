<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		$this->call('StudentsTableSeeder');
		$this->call('FeesTableSeeder');
		$this->call('ExamsTableSeeder');
		$this->call('LibraryBooksTableSeeder');
	}

}

class StudentsTableSeeder extends Seeder
{
		    public function run()
		    {  
				$seed_data = Faker\Factory::create();
			    for ($count=1; $count < 100 ; $count++) {
				    
				    $fname= $seed_data->firstName;
				    $sname= $seed_data->lastName;
				    
				    if ($count < 10) {
				    	$admission='000'.$count;
				    }
				    else{
				    	$admission='00'.$count;
				    }

				    DB::table('students')->insert(array(
				    		'created_at'		=> $seed_data->dateTimeThisMonth($max = 'now'),
							'first_name'		=> $fname, 
							'last_name'			=> $sname, 
							'user_name' 		=> $sname.'.'.$fname.'@students.jkuat', 
							'password' 			=> Hash::make($fname), 
							'admission_number'	=> 'CS282-'.$admission.'/2010', 
							'department'		=> $seed_data->company, 
							'faculty'			=> $seed_data->company, 
							'course' 			=> $seed_data->company, 
							));
			    }
			}	
}

class FeesTableSeeder extends Seeder
{
    public function run()
			    {
			    	$seed_data = Faker\Factory::create();
			    	for($v=1; $v < 100; $v++){
					    	for ($i=1; $i < 10; $i++) { //payments made by the students
					    			$type=='RECEIPT';
					    			$transaction_number='RCT';
					    			$desc='Fee collection ';
					    		
					    		DB::table('fees')->insert(array(
								'student_id'		=>$v, 
								'date'  			=>$seed_data->dateTimeThisMonth($max = 'now'),
								'type'				=>$type, 
								'transaction_number'=>$transaction_number.$seed_data->numberBetween($min = 1000000, $max = 9000000), 
								'description'		=>$desc.$seed_data->paragraph($nbSentences = 3) , 
								'amount'  			=>$seed_data->numberBetween($min = 2000, $max = 20000), 
								'created_at'		=>$seed_data->dateTimeThisMonth($max = 'now'), 
								));
					    	}
			    	}
			    	for ($i=1; $i < 100; $i++) { //payments made by the students for helb
			    		for($cnt=1; $cnt < 2; $cnt++)
			    		{
			    			$type='ADJUSTMENT';
				    		$transaction_number='ADJ';
				    		$desc='Helb bursary ';
		
				    		DB::table('fees')->insert(array(
							'student_id'		=>$i, 
							'date'  			=>$seed_data->dateTimeThisMonth($max = 'now'),
							'type'				=>$type, 
							'transaction_number'=>$transaction_number.$seed_data->numberBetween($min = 1000000, $max = 9000000), 
							'description'		=>$desc.$seed_data->paragraph($nbSentences = 3) , 
							'amount'  			=>$seed_data->numberBetween($min = 2000, $max = 20000), 
							'created_at'		=>$seed_data->dateTimeThisMonth($max = 'now'), 
							));
			    		}
			    	}

			    	for ($i=1; $i < 100; $i++) { //invoice each student
			    		DB::table('fees')->insert(array(
						'student_id'		=>$i, 
						'date'  			=>$seed_data->dateTimeThisMonth($max = 'now'),
						'type'				=>'INVOICE', 
						'transaction_number'=>'IN0'.$seed_data->numberBetween($min = 1000000, $max = 9000000), 
						'description'		=>'17-ICSIT '.$seed_data->paragraph($nbSentences = 3) , 
						'amount'  			=>20250.00, 
						'created_at'		=>$seed_data->dateTimeThisMonth($max = 'now'), 
						));
			    	}
				}
}

class ExamsTableSeeder extends Seeder
{
    public function run() {
			    	$seed_data = Faker\Factory::create();
			    	$units = array('BIT 2102' => 'Computer Hardware and OrganizationBIT 2102',
			    					'BIT 2205'=>'Networking Essentials',
			    					'BCT 2101'=>'Introduction to Computers and Operating Systems',
			    					'BCT 2102'=>'Software Applications-I',
			    					'BCT 2103'=>'Software Applications-II',
			    					'BCT 2104'=>'Principles of Programming Languages',
			    					'BCT 2105'=>'Accounting Software',
			    					'BCT 2106'=>'Internet',
			    					'SMA 2104'=>'Mathematics for Sciences',
			    					'BIT 2104'=>'Introduction to Programming',
			    					'BIT 2106'=>'Operating Systems I',
			    					'BIT 2109'=>'Object Oriented Programming I',
			    					'BIT 2116'=>'Network System Design and Implementation',
			    					'BCT 2201'=>'Computing and Communication Skills',
			    					'BCT 2202'=>'Principles of Electrical Engineering.',
			    					'BCT 2203'=>'Principles of Computer Maintenance',
			    					'SZL 2111'=>'HIV AIDS',
			    					'BCT 2204'=>'Entrepreneurial Skills',
								);
			    	$date=$seed_data->dateTimeThisMonth($max = 'now');
			    	for ($i=1; $i < 100; $i++) { 
			    		foreach ($units as $unit => $u_name) {
			    			DB::table('exams')->insert(array(
														'student_id'	=>$i, 
														'year_of_study' =>'Year 1', 
														'unit_code'		=>$unit,
														'unit_name'		=>$u_name,
														'grade'		 	=>$seed_data->randomElement($array = array ('A','B','C','D','E')), 
														'created_at' 	=>$date,  
														));
			    		}
			    	}
				}
}


class LibraryBooksTableSeeder extends Seeder
{
    public function run()
			    {
			    	$seed_data = Faker\Factory::create();
			    	for ($i=0; $i < 1000; $i++) 
			    	{ 
			    		DB::table('librarybooks')->insert(array(
						'student_id' 	=>$seed_data->numberBetween($min = 1, $max = 99), 
						'book_name'		=>$seed_data->sentence($nbWords = 3), 
						'book_author' 	=>$seed_data->name, 
						'period_allowed'=>$seed_data->numberBetween($min = 1, $max = 14), 
						'created_at' 	=>$seed_data->dateTimeThisMonth($max = 'now'),  
						));
			    	}
				}
}